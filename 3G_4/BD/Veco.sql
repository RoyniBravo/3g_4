create database veco
DEFAULT CHARACTER SET utf8;
use veco;

create table ciudades (
idciudad int not null unique auto_increment,
nombrec varchar(30) not null,
descripcionc  text not null,
primary key(idciudad)
);

create table paises (
idpais int not null unique auto_increment,
nombrep varchar(30) not null,
descripcionp  text not null,
primary key(idpais)
);

CREATE TABLE clientes(
	idclientes INT NOT NULL UNIQUE AUTO_INCREMENT,
	username VARCHAR(25) UNIQUE NOT NULL,
    rango VARCHAR(25),
	email VARCHAR(255) UNIQUE NOT NULL,
    ciudad VARCHAR(255),
    pais VARCHAR(255),
	password VARCHAR(255) NOT NULL,
	fecharegistro DATETIME NOT NULL,
	PRIMARY KEY(idclientes),
    FOREIGN KEY(ciudad)
            REFERENCES ciudades(idciudad) 
            ON UPDATE CASCADE 
            ON DELETE RESTRICT,
    FOREIGN KEY(pais)
            REFERENCES paises(idpais) 
            ON UPDATE CASCADE 
            ON DELETE RESTRICT
);

CREATE TABLE empleado(
	idempleado INT NOT NULL UNIQUE AUTO_INCREMENT,
	username VARCHAR(25) UNIQUE NOT NULL,
    rango VARCHAR(25),
	email VARCHAR(255) UNIQUE NOT NULL,
    ciudad VARCHAR(255),
    pais VARCHAR(255),
	password VARCHAR(255) NOT NULL,
	fecharegistro DATETIME NOT NULL,
	PRIMARY KEY(idempleado),
    FOREIGN KEY(ciudad)
            REFERENCES ciudades(idciudad) 
            ON UPDATE CASCADE 
            ON DELETE RESTRICT,
    FOREIGN KEY(pais)
            REFERENCES paises(idpais) 
            ON UPDATE CASCADE 
            ON DELETE RESTRICT
);

CREATE TABLE administrador(
	idadmin INT NOT NULL UNIQUE AUTO_INCREMENT,
	username VARCHAR(25) UNIQUE NOT NULL,
    rango VARCHAR(25),
	email VARCHAR(255) UNIQUE NOT NULL,
    ciudad VARCHAR(255),
    pais VARCHAR(255),
	password VARCHAR(255) NOT NULL,
	fecharegistro DATETIME NOT NULL,
	PRIMARY KEY(idadmin),
    FOREIGN KEY(ciudad)
            REFERENCES ciudades(idciudad) 
            ON UPDATE CASCADE 
            ON DELETE RESTRICT,
    FOREIGN KEY(pais)
            REFERENCES paises(idpais) 
            ON UPDATE CASCADE 
            ON DELETE RESTRICT
);


create table pedido (
idpedido int not null unique auto_increment,
iduser int not null, 
nombrep varchar(30) not null,
descripcionp  text not null,
fechapedido DATETIME not null,
entregado tinyint not null,
primary key(idpedido),
FOREIGN KEY(iduser)
            REFERENCES usuarios(id) 
            ON UPDATE CASCADE 
            ON DELETE RESTRICT
);