/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crud;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Cris
 */
@Stateless
public class AdministradorFacade extends AbstractFacade<Administrador> {
    
    public Administrador connect = null;

    @PersistenceContext(unitName = "3G_4PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
    public boolean connect(String username, String password) {
        try {
            connect = this.getEntityManager().createNamedQuery("Administrador.control", Administrador.class).setParameter("username", username).setParameter("password", password).getSingleResult();
            if(connect != null) {
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    public AdministradorFacade() {
        super(Administrador.class);
    }
    
}
