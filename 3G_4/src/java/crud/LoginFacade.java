/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package crud;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import crud.Administrador;

/**
 *
 * @author Kike
 */
@Stateless
public class LoginFacade extends AbstractFacade<Administrador> {

    @PersistenceContext(unitName = "3G_4PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public LoginFacade() {
        super(Administrador.class);
    }
    
}
